import * as React from 'react';
import {Button, Text, View} from 'react-native';
import {Utils} from '../utils/Utils';
import {AsyncStorage} from 'react-native';

class Login extends React.Component<any, any> {
  login = async () => {
    const user = {loggedIn: true};
    await AsyncStorage.setItem(Utils.userKey, JSON.stringify(user));
    this.props.navigation.navigate(Utils.screenNames.APP_STACK);
  };
  render() {
    return (
      <View>
        <Text>This is my Login Screen</Text>
        <Button title={'Login'} onPress={this.login} />
      </View>
    );
  }
}

export default Login;
