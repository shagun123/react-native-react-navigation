import * as React from 'react';
import {ActivityIndicator, AsyncStorage} from 'react-native';
import {Utils} from '../utils/Utils';

export class AuthLoading extends React.Component<any, any> {
  getUser() {
    return AsyncStorage.getItem(Utils.userKey).then(data => {
      return JSON.parse(data);
    });
  }
  async componentDidMount() {
    const user = await this.getUser();
    if (user) {
      this.props.navigation.navigate(
        user.loggedIn
          ? Utils.screenNames.APP_STACK
          : Utils.screenNames.AUTH_STACK,
      );
    } else {
      this.props.navigation.navigate(Utils.screenNames.AUTH_STACK);
    }
  }

  render() {
    return <ActivityIndicator size={'large'} />;
  }
}
