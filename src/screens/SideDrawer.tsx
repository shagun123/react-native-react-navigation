import * as React from 'react';
import {Text, View} from 'react-native';

class SideDrawer extends React.Component<any, any> {
  render() {
    return (
      <View>
        <Text>This is my Side Drawer Screen</Text>
      </View>
    );
  }
}

export default SideDrawer;
