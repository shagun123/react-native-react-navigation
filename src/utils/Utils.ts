enum ScreenNames {
  LOGIN = 'Login',
  MORE = 'More',
  PHOTOS = 'Photos',
  HOME = 'Home',
  TAB = 'Tab',
  ENSURE_AUTH = 'EnsureAuth',
  AUTH_STACK = 'AuthSTack',
  APP_STACK = 'AppSTack'
}

export class Utils {
  static readonly screenNames = ScreenNames;
  static readonly userKey ='USER'
}
