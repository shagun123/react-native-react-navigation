import {createAppContainer} from 'react-navigation';
import {appStackWithAuth} from './Routes';

const Navigator = createAppContainer(appStackWithAuth);

export default Navigator;
