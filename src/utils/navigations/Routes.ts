import {createStackNavigator} from 'react-navigation-stack';
import Login from '../../screens/Login';
import {Utils} from '../Utils';
import {createDrawerNavigator} from 'react-navigation-drawer';
import SideDrawer from '../../screens/SideDrawer';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import Home from '../../screens/Home';
import More from '../../screens/More';
import Photos from '../../screens/Photos';
import {createSwitchNavigator} from 'react-navigation';
import {AuthLoading} from '../../screens/AuthLoading';
const authStack = createStackNavigator(
  {
    [Utils.screenNames.LOGIN]: Login,
  },
  {
    navigationOptions: {
      header: null,
    },
  },
);

const tabStack = createBottomTabNavigator(
  {
    [Utils.screenNames.HOME]: Home,
    [Utils.screenNames.MORE]: More,
    [Utils.screenNames.PHOTOS]: Photos,
  },
  {
    initialRouteName: Utils.screenNames.HOME,
  },
);

const appStack = createDrawerNavigator(
  {
    [Utils.screenNames.TAB]: tabStack,
  },
  {
    initialRouteName: Utils.screenNames.TAB,
    backBehavior: 'history',
    contentComponent: SideDrawer,
    drawerWidth: 300,
  },
);

const appStackWithAuth = createSwitchNavigator(
  {
    [Utils.screenNames.APP_STACK]: appStack,
    [Utils.screenNames.AUTH_STACK]: authStack,
    [Utils.screenNames.ENSURE_AUTH]: AuthLoading,
  },
  {
    initialRouteName: Utils.screenNames.ENSURE_AUTH,
  },
);

export {appStackWithAuth};
